var Panel = ReactBootstrap.Panel;
var PanelGroup = ReactBootstrap.PanelGroup;
var Button = ReactBootstrap.Button;
var FormGroup = ReactBootstrap.FormGroup;
var FormControl = ReactBootstrap.FormControl;
var ControlLabel = ReactBootstrap.ControlLabel;

var authors = [];


class InitForm extends React.Component{
    constructor(props){
        super(props);
        this.formInput = "";
    }
    
    submit(event){
        event.preventDefault();
        var orcids = this.formInput.value;
        
        $.ajax({
            type: 'get',
            url: 'http://localhost:8081/',
            data: {orcids: orcids},
            success: function(data) {
                authors = JSON.parse(data);
                
                for(let i = 0; i < authors.length; i++){
                    for(let j = 0; j < authors[i].works.length; j++){
                        authors[i].works[j].sjr = [];
                        authors[i].works[j].pub_titles = [];
                        for(let k = 0; k < authors[i].works[j].issn.length; k++){
                            let l = authors[i].pubs.issn.indexOf(authors[i].works[j].issn[k]);
                            if(l == -1) continue;
                            else{
                                authors[i].works[j].sjr[k] = authors[i].pubs.sjr[l];
                                authors[i].works[j].pub_titles[k] = authors[i].pubs.titles[l];
                            }
                        }
                    }
                }
                console.log(authors);
                ReactDOM.render(
                    <ResultPanelGroup />,
                    document.getElementById("result-div")
                );
            }
        });
        

        ReactDOM.render(
            <ResultPanelGroup />,
            document.getElementById("result-div")
        );
    }
    
    render(){
        return (
            <div id="init-div" className="col-md-6">
                <form id="initForm" onSubmit={this.submit.bind(this)}>
                    <FormGroup bsSize="small">
                        <ControlLabel>Insert one or more OrcIDs separated by comma (,)</ControlLabel>
                        <FormControl inputRef={ref => { this.formInput = ref; }} componentClass="textarea" style={{ height: 75 }} placeholder="Enter text"/>
                        <Button className="btn btn-primary btn-large centerButton" type="submit">Submit</Button>
                    </FormGroup>
                </form>
            </div>
        );
    }
}


class ResultPanelGroup extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleSelect = this.handleSelect.bind(this);

    this.state = {
      activeKey: '1'
    };
  }

  handleSelect(activeKey) {
    this.setState({ activeKey });
  }

    render() {
        return (
            <PanelGroup accordion id="accordion-uncontrolled-example" defaultActiveKey="1">
            {
            authors.map((currentValue,index) => 
                <Panel eventKey={index+1}>
                    <Panel.Heading>
                        <Panel.Title toggle>{currentValue["author"]["name"] + " - " + currentValue["author"]["orcid"]}</Panel.Title>
                    </Panel.Heading>
                    {
                        currentValue.works.map((currentValue2, index2) =>
                                    <Panel.Body collapsible style={{backgroundColor:"#F8F8F8"}}>
                                    {currentValue2.title}
                                    {currentValue2["put_codes"].map(function (currentValue3, index3){
                                        
                                        if(currentValue2.scopus_ids[index3] == null) currentValue2.scopus_ids[index3] = "NA";
                                        if(currentValue2.dblp_ids[index3] == null) currentValue2.dblp_ids[index3] = "NA";
                                                                    
                                        let space = " ";                            
                                        if(currentValue2.issn[index3] != null) 
                                            return <div style={{backgroundColor:"#F8F8F8"}}> 
                                            <Panel.Body collapsible style={{backgroundColor:"white"}}>
                                            <pre>
                                            {"Scopus: " + currentValue2.scopus_ids[index3] + "     WOS: "+ currentValue2.wos_ids[index3] +"      DBLP: " + currentValue2.dblp_ids[index3] + "     Type: " + currentValue2.types[index3] + "     Year: " + currentValue2.years[index3]}
                                            </pre>
                                            <Panel.Body collapsible style={{backgroundColor:"white"}}>
                                            <pre>
                                            {"ISSN: " + currentValue2.issn[index3] + "      Title: " + currentValue2.pub_titles[index3] + "   (SJR: " + currentValue2.sjr[index3] + ") "}
                                            </pre>
                                            </Panel.Body> 
                                            </Panel.Body>
                                            </div>
                                            
                                        
                                        else return <Panel.Body collapsible style={{backgroundColor:"white"}}>
                                                    <pre>
                                                    {"Scopus: " + currentValue2.scopus_ids[index3] + "     WOS: "+ currentValue2.wos_ids[index3] +"     DBLP: " + currentValue2.dblp_ids[index3] + "     Type: " + currentValue2.types[index3] + "      Year: " + currentValue2.years[index3]}
                                                    </pre>
                                                    </Panel.Body>;
                                    
                                        
                                                                    
                                    }
                                    )}
                                    </Panel.Body>
                        )
                    }
                </Panel>
            )
            }
            </PanelGroup>
        );
    }
}

ReactDOM.render(
  <InitForm />,
  document.getElementById("form-div")
);
