var Panel = ReactBootstrap.Panel;
var PanelGroup = ReactBootstrap.PanelGroup;
var Button = ReactBootstrap.Button;
var FormGroup = ReactBootstrap.FormGroup;
var FormControl = ReactBootstrap.FormControl;
var ControlLabel = ReactBootstrap.ControlLabel;

var authors = [];

class InitForm extends React.Component {
    constructor(props) {
        super(props);
        this.formInput = "";
    }

    submit(event) {
        event.preventDefault();
        var orcids = this.formInput.value;

        $.ajax({
            type: 'get',
            url: 'http://localhost:8081/',
            data: { orcids: orcids },
            success: function (data) {
                authors = JSON.parse(data);

                for (let i = 0; i < authors.length; i++) {
                    for (let j = 0; j < authors[i].works.length; j++) {
                        authors[i].works[j].sjr = [];
                        authors[i].works[j].pub_titles = [];
                        for (let k = 0; k < authors[i].works[j].issn.length; k++) {
                            let l = authors[i].pubs.issn.indexOf(authors[i].works[j].issn[k]);
                            if (l == -1) continue;else {
                                authors[i].works[j].sjr[k] = authors[i].pubs.sjr[l];
                                authors[i].works[j].pub_titles[k] = authors[i].pubs.titles[l];
                            }
                        }
                    }
                }
                console.log(authors);
                ReactDOM.render(React.createElement(ResultPanelGroup, null), document.getElementById("result-div"));
            }
        });

        ReactDOM.render(React.createElement(ResultPanelGroup, null), document.getElementById("result-div"));
    }

    render() {
        return React.createElement(
            'div',
            { id: 'init-div', className: 'col-md-6' },
            React.createElement(
                'form',
                { id: 'initForm', onSubmit: this.submit.bind(this) },
                React.createElement(
                    FormGroup,
                    { bsSize: 'small' },
                    React.createElement(
                        ControlLabel,
                        null,
                        'Insert one or more OrcIDs separated by comma (,)'
                    ),
                    React.createElement(FormControl, { inputRef: ref => {
                            this.formInput = ref;
                        }, componentClass: 'textarea', style: { height: 75 }, placeholder: 'Enter text' }),
                    React.createElement(
                        Button,
                        { className: 'btn btn-primary btn-large centerButton', type: 'submit' },
                        'Submit'
                    )
                )
            )
        );
    }
}

class ResultPanelGroup extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            activeKey: '1'
        };
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    render() {
        return React.createElement(
            PanelGroup,
            { accordion: true, id: 'accordion-uncontrolled-example', defaultActiveKey: '1' },
            authors.map((currentValue, index) => React.createElement(
                Panel,
                { eventKey: index + 1 },
                React.createElement(
                    Panel.Heading,
                    null,
                    React.createElement(
                        Panel.Title,
                        { toggle: true },
                        currentValue["author"]["name"] + " - " + currentValue["author"]["orcid"]
                    )
                ),
                currentValue.works.map((currentValue2, index2) => React.createElement(
                    Panel.Body,
                    { collapsible: true, style: { backgroundColor: "#F8F8F8" } },
                    currentValue2.title,
                    currentValue2["put_codes"].map(function (currentValue3, index3) {

                        if (currentValue2.scopus_ids[index3] == null) currentValue2.scopus_ids[index3] = "NA";
                        if (currentValue2.dblp_ids[index3] == null) currentValue2.dblp_ids[index3] = "NA";

                        let space = " ";
                        if (currentValue2.issn[index3] != null) return React.createElement(
                            'div',
                            { style: { backgroundColor: "#F8F8F8" } },
                            React.createElement(
                                Panel.Body,
                                { collapsible: true, style: { backgroundColor: "white" } },
                                React.createElement(
                                    'pre',
                                    null,
                                    "Scopus: " + currentValue2.scopus_ids[index3] + "     WOS: " + currentValue2.wos_ids[index3] + "      DBLP: " + currentValue2.dblp_ids[index3] + "     Type: " + currentValue2.types[index3] + "     Year: " + currentValue2.years[index3]
                                ),
                                React.createElement(
                                    Panel.Body,
                                    { collapsible: true, style: { backgroundColor: "white" } },
                                    React.createElement(
                                        'pre',
                                        null,
                                        "ISSN: " + currentValue2.issn[index3] + "      Title: " + currentValue2.pub_titles[index3] + "   (SJR: " + currentValue2.sjr[index3] + ") "
                                    )
                                )
                            )
                        );else return React.createElement(
                            Panel.Body,
                            { collapsible: true, style: { backgroundColor: "white" } },
                            React.createElement(
                                'pre',
                                null,
                                "Scopus: " + currentValue2.scopus_ids[index3] + "     WOS: " + currentValue2.wos_ids[index3] + "     DBLP: " + currentValue2.dblp_ids[index3] + "     Type: " + currentValue2.types[index3] + "      Year: " + currentValue2.years[index3]
                            )
                        );
                    })
                ))
            ))
        );
    }
}

ReactDOM.render(React.createElement(InitForm, null), document.getElementById("form-div"));
