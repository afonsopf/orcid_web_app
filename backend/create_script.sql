--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-06-17 13:01:37

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 24809)
-- Name: orcid; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA orcid;


ALTER SCHEMA orcid OWNER TO postgres;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2846 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 24810)
-- Name: author; Type: TABLE; Schema: orcid; Owner: postgres
--

CREATE TABLE orcid.author (
    author_id integer NOT NULL,
    orcid character varying(20) NOT NULL,
    author_name text NOT NULL,
    last_update bigint NOT NULL
);


ALTER TABLE orcid.author OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24816)
-- Name: author_author_id_seq; Type: SEQUENCE; Schema: orcid; Owner: postgres
--

CREATE SEQUENCE orcid.author_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE orcid.author_author_id_seq OWNER TO postgres;

--
-- TOC entry 2848 (class 0 OID 0)
-- Dependencies: 198
-- Name: author_author_id_seq; Type: SEQUENCE OWNED BY; Schema: orcid; Owner: postgres
--

ALTER SEQUENCE orcid.author_author_id_seq OWNED BY orcid.author.author_id;


--
-- TOC entry 199 (class 1259 OID 24818)
-- Name: publication; Type: TABLE; Schema: orcid; Owner: postgres
--

CREATE TABLE orcid.publication (
    issn character varying(10) NOT NULL,
    title text,
    sjr real
);


ALTER TABLE orcid.publication OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 24824)
-- Name: work; Type: TABLE; Schema: orcid; Owner: postgres
--

CREATE TABLE orcid.work (
    work_id integer NOT NULL,
    title text NOT NULL
);


ALTER TABLE orcid.work OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 24827)
-- Name: work_author; Type: TABLE; Schema: orcid; Owner: postgres
--

CREATE TABLE orcid.work_author (
    author integer NOT NULL,
    parent_work integer NOT NULL
);


ALTER TABLE orcid.work_author OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24830)
-- Name: work_publication; Type: TABLE; Schema: orcid; Owner: postgres
--

CREATE TABLE orcid.work_publication (
    put_code character varying(20) NOT NULL,
    parent_work integer NOT NULL,
    pub_year integer,
    scopus_id character varying(40),
    dblp_id character varying(40),
    wos_id character varying(20),
    pub_issn character varying(10),
    cit_2016 integer,
    cit_2017 integer,
    cit_2018 integer,
    cit_total integer,
    pub_type character varying(20)
);


ALTER TABLE orcid.work_publication OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 24881)
-- Name: work_work_id_seq; Type: SEQUENCE; Schema: orcid; Owner: postgres
--

CREATE SEQUENCE orcid.work_work_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE orcid.work_work_id_seq OWNER TO postgres;

--
-- TOC entry 2854 (class 0 OID 0)
-- Dependencies: 203
-- Name: work_work_id_seq; Type: SEQUENCE OWNED BY; Schema: orcid; Owner: postgres
--

ALTER SEQUENCE orcid.work_work_id_seq OWNED BY orcid.work.work_id;


--
-- TOC entry 2693 (class 2604 OID 24836)
-- Name: author author_id; Type: DEFAULT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.author ALTER COLUMN author_id SET DEFAULT nextval('orcid.author_author_id_seq'::regclass);


--
-- TOC entry 2694 (class 2604 OID 24883)
-- Name: work work_id; Type: DEFAULT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.work ALTER COLUMN work_id SET DEFAULT nextval('orcid.work_work_id_seq'::regclass);


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 198
-- Name: author_author_id_seq; Type: SEQUENCE SET; Schema: orcid; Owner: postgres
--

SELECT pg_catalog.setval('orcid.author_author_id_seq', 3491, true);


--
-- TOC entry 2857 (class 0 OID 0)
-- Dependencies: 203
-- Name: work_work_id_seq; Type: SEQUENCE SET; Schema: orcid; Owner: postgres
--

SELECT pg_catalog.setval('orcid.work_work_id_seq', 80306, true);


--
-- TOC entry 2696 (class 2606 OID 24838)
-- Name: author author_pkey; Type: CONSTRAINT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (author_id);


--
-- TOC entry 2698 (class 2606 OID 24840)
-- Name: publication publication_pkey; Type: CONSTRAINT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.publication
    ADD CONSTRAINT publication_pkey PRIMARY KEY (issn);


--
-- TOC entry 2700 (class 2606 OID 24842)
-- Name: work work_pkey; Type: CONSTRAINT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.work
    ADD CONSTRAINT work_pkey PRIMARY KEY (work_id);


--
-- TOC entry 2705 (class 2606 OID 24844)
-- Name: work_publication work_publication_pkey; Type: CONSTRAINT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.work_publication
    ADD CONSTRAINT work_publication_pkey PRIMARY KEY (put_code);


--
-- TOC entry 2702 (class 1259 OID 24845)
-- Name: fki_parent_work_fk; Type: INDEX; Schema: orcid; Owner: postgres
--

CREATE INDEX fki_parent_work_fk ON orcid.work_publication USING btree (parent_work);


--
-- TOC entry 2703 (class 1259 OID 24846)
-- Name: fki_publicationISSN_fk; Type: INDEX; Schema: orcid; Owner: postgres
--

CREATE INDEX "fki_publicationISSN_fk" ON orcid.work_publication USING btree (pub_issn);


--
-- TOC entry 2701 (class 1259 OID 24847)
-- Name: fki_work_author_parent_fk; Type: INDEX; Schema: orcid; Owner: postgres
--

CREATE INDEX fki_work_author_parent_fk ON orcid.work_author USING btree (parent_work);


--
-- TOC entry 2708 (class 2606 OID 24848)
-- Name: work_publication parent_work_fk; Type: FK CONSTRAINT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.work_publication
    ADD CONSTRAINT parent_work_fk FOREIGN KEY (parent_work) REFERENCES orcid.work(work_id);


--
-- TOC entry 2709 (class 2606 OID 24853)
-- Name: work_publication publicationISSN_fk; Type: FK CONSTRAINT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.work_publication
    ADD CONSTRAINT "publicationISSN_fk" FOREIGN KEY (pub_issn) REFERENCES orcid.publication(issn);


--
-- TOC entry 2706 (class 2606 OID 24858)
-- Name: work_author work_author_author_fkey; Type: FK CONSTRAINT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.work_author
    ADD CONSTRAINT work_author_author_fkey FOREIGN KEY (author) REFERENCES orcid.author(author_id);


--
-- TOC entry 2707 (class 2606 OID 24863)
-- Name: work_author work_author_parent_fk; Type: FK CONSTRAINT; Schema: orcid; Owner: postgres
--

ALTER TABLE ONLY orcid.work_author
    ADD CONSTRAINT work_author_parent_fk FOREIGN KEY (parent_work) REFERENCES orcid.work(work_id);


--
-- TOC entry 2844 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA orcid; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA orcid TO orcid_client;


--
-- TOC entry 2847 (class 0 OID 0)
-- Dependencies: 197
-- Name: TABLE author; Type: ACL; Schema: orcid; Owner: postgres
--

GRANT ALL ON TABLE orcid.author TO orcid_client;


--
-- TOC entry 2849 (class 0 OID 0)
-- Dependencies: 198
-- Name: SEQUENCE author_author_id_seq; Type: ACL; Schema: orcid; Owner: postgres
--

GRANT ALL ON SEQUENCE orcid.author_author_id_seq TO orcid_client;


--
-- TOC entry 2850 (class 0 OID 0)
-- Dependencies: 199
-- Name: TABLE publication; Type: ACL; Schema: orcid; Owner: postgres
--

GRANT ALL ON TABLE orcid.publication TO orcid_client;


--
-- TOC entry 2851 (class 0 OID 0)
-- Dependencies: 200
-- Name: TABLE work; Type: ACL; Schema: orcid; Owner: postgres
--

GRANT ALL ON TABLE orcid.work TO orcid_client;


--
-- TOC entry 2852 (class 0 OID 0)
-- Dependencies: 201
-- Name: TABLE work_author; Type: ACL; Schema: orcid; Owner: postgres
--

GRANT ALL ON TABLE orcid.work_author TO orcid_client;


--
-- TOC entry 2853 (class 0 OID 0)
-- Dependencies: 202
-- Name: TABLE work_publication; Type: ACL; Schema: orcid; Owner: postgres
--

GRANT ALL ON TABLE orcid.work_publication TO orcid_client;


--
-- TOC entry 2855 (class 0 OID 0)
-- Dependencies: 203
-- Name: SEQUENCE work_work_id_seq; Type: ACL; Schema: orcid; Owner: postgres
--

GRANT ALL ON SEQUENCE orcid.work_work_id_seq TO orcid_client;


-- Completed on 2018-06-17 13:01:37

--
-- PostgreSQL database dump complete
--

