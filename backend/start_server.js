var fs = require("fs"), readLine = require("readline");
const request = require("request");
const async = require("async");
const {Client} = require("pg");

const express = require('express');
const app = express();


var orcid_access_token = "4ceb92be-20fd-45a8-97c2-ef5270815945";
var elsevier_api_key = "e8f9469201755d77fb48095aa9a8cb5d";

function fetch_from_api_orcid(author, callback){
    var orcid = author.orcid;
    var works = []
    var response_data;
    
    request({
        url: "https://pub.orcid.org/v2.1/"+orcid+"/works/",
        method: "GET",
        headers: {Accept: "application/json", Authorization:"Bearer "+orcid_access_token} //vnd.orcid+xml
    },  function (error, response, body){
        
            if(response.statusCode != 200){ 
                console.error("Error " + response.statusCode + " on fetching ORCID 2");
                callback(response.statusCode);
                return;
            };
            
            response_data = JSON.parse(body)["group"];
            var  w, scop, dblp, wos, issn;

            for(let i = 0; i<response_data.length; i++){
                w = new Object();
                w.years = [];
                w.put_codes = [];
                w.types = [];
                w.scopus_ids = [];
                w.dblp_ids = [];
                w.wos_ids = [];
                w.issn = [];
                
                w.title = response_data[i]["work-summary"][0]["title"]["title"]["value"];

                for(let j=0; j<response_data[i]["work-summary"].length; j++){
                    
                    w.put_codes.push(response_data[i]["work-summary"][j]["put-code"]);
                    w.types.push(response_data[i]["work-summary"][j]["type"]);
                    if(response_data[i]["work-summary"][j]["publication-date"] != null) w.years.push(response_data[i]["work-summary"][j]["publication-date"]["year"]["value"]);
                    else w.years.push(undefined);
                    
                    scop = undefined, dblp = undefined, wos = undefined, issn = undefined;

                    if(response_data[i]["work-summary"][j]["external-ids"] != undefined){
                        for(let k=0; k < response_data[i]["work-summary"][j]["external-ids"]["external-id"].length; k++){
                            if(response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-type"] == "eid") scop = response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-value"];
                       
                            if(response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-type"] == "other-id")
                                if(response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-value"] != null)
                                    if(response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-value"].startsWith("dblp"))
                                        dblp = response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-value"];
            
                            if(response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-type"] == "wosuid") wos = response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-value"];
                        
                            if(response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-type"] == "issn") issn = response_data[i]["work-summary"][j]["external-ids"]["external-id"][k]["external-id-value"];
                        }
                    }
                    w.scopus_ids.push(scop);
                    w.dblp_ids.push(dblp);
                    w.wos_ids.push(wos);
                    w.issn.push(issn);
                }
                works.push(w);
            }
            var data = new Object();
            data.author = author;
            data.works = works;
            fetch_from_api_issn(data, callback);
    });
}


async function fetch_from_api_issn(data, callback){
    var issn_list = [];
    var pubs = new Object();
    pubs.titles = [];
    pubs.issn = [];
    pubs.sjr = [];
    
    for(let i = 0; i < data.works.length; i++){
        for(let j = 0; j < data.works[i]["issn"].length; j++){
            if(data.works[i]["issn"][j] == undefined) continue;
            else issn_list.push(data.works[i]["issn"][j]);
        }
    }

    issn_list = Array.from(new Set(issn_list));
    pubs.issn = issn_list;
    
    issn_slices = [];
    for(let i = 0; i <issn_list.length; i = i+25){
        issn_slices.push(issn_list.slice(i , i+25));
    }
    
   async.each(issn_slices, function(item, callback){
       let this_issn_list = item.toString();
       request({
        url: "https://api.elsevier.com/content/serial/title/issn/"+this_issn_list,
        method: "GET",
        headers: {Accept: "application/json", "X-ELS-APIKey": elsevier_api_key },
        }, function (error, response, body){
            
            if(response.statusCode != 200){ 
                console.error("Error " + response.statusCode + " ISSN");
                callback(error);
                return;
            }
            
            response_data = JSON.parse(body);
            let title, sjr, issn;
            for(let i = 0; i < response_data["serial-metadata-response"]["entry"].length; i++){
                let j = issn_list.indexOf(response_data["serial-metadata-response"]["entry"][i]["prism:issn"]);
                if(j == -1) j = issn_list.indexOf(response_data["serial-metadata-response"]["entry"][i]["prism:eIssn"]);
                if(j == -1) continue;
               
                if(response_data["serial-metadata-response"]["entry"][i]["SJRList"] != undefined) pubs.sjr[j] = response_data["serial-metadata-response"]["entry"][i]["SJRList"]["SJR"][0]["$"];
                
                pubs.titles[j] = response_data["serial-metadata-response"]["entry"][i]["dc:title"];
                
            }
            callback();
    });
    }, function(err){
        data.pubs = pubs;
        store_local(data);
        
        if(err != null) callback(err, data);
        else callback(null, data);
        
    });
}


function fetch_from_local(author, callback){
    
    var client = new Client({
        user: "orcid_client",
        host: "127.0.0.1",
        database: "orcid",
        password: "orcidpwd",
        port: "5432",
    })
    var author_id = 0;
    var work_ids = [];
    var data = new Object();
    data.author = author;
    data.works = [];
    
    client.connect()
    client.query('SELECT author_id FROM orcid.author WHERE orcid=$1', [author.orcid], (err, res) => {
        if(res.rows.length == 0){ callback("author not found"); return;}
        else author_id = res.rows[0].author_id;
                 
        let query_text = "SELECT DISTINCT orcid.publication.issn, orcid.publication.title, orcid.publication.sjr FROM orcid.publication INNER JOIN orcid.work_publication ON orcid.publication.issn = orcid.work_publication.pub_issn INNER JOIN orcid.work ON orcid.work_publication.parent_work = orcid.work.work_id INNER JOIN orcid.work_author ON orcid.work.work_id = orcid.work_author.parent_work WHERE orcid.work_author.author = $1"; 
        var pubs = new Object();
        pubs.titles = [];
        pubs.issn = [];
        pubs.sjr = [];
        client.query(query_text, [author_id], (err, res) => {
            for(let i = 0; i < res.rows.length; i++){
                pubs.titles.push(res.rows[i].title);
                pubs.issn.push(res.rows[i].issn);
                pubs.sjr.push(res.rows[i].sjr);
            }
            data.pubs = pubs;
        });
        query_text = "SELECT work.work_id, work.title FROM orcid.work INNER JOIN orcid.work_author ON orcid.work.work_id = orcid.work_author.parent_work WHERE orcid.work_author.author = $1";
        client.query(query_text, [author_id], (err, res) => {
            if(res.rows.length == 0){
                callback(null, data);
                return;
            }
            let aux_res_count = res.rows.length;
            query_text = "SELECT put_code, pub_year, scopus_id, dblp_id, wos_id, pub_issn, pub_type FROM orcid.work_publication WHERE parent_work = $1";
            for(let i = 0; i < res.rows.length; i++){
                let aux_work_id = res.rows[i].work_id;
                let aux_work_title = res.rows[i].title;
                client.query(query_text, [aux_work_id], (err, res) => {
                   let w = new Object();
                   w.title = aux_work_title;
                   w.put_codes = [];
                   w.years = [];
                   w.scopus_ids = [];
                   w.dblp_ids = [];
                   w.wos_ids = [];
                   w.issn = [];
                   w.types = [];
                   
                   for(let j = 0; j < res.rows.length; j++){
                        w.put_codes.push(res.rows[j].put_code);
                        w.years.push(res.rows[j].pub_year);
                        w.scopus_ids.push(res.rows[j].scopus_id);
                        w.dblp_ids.push(res.rows[j].dblp_id);
                        w.wos_ids.push(res.rows[j].wos_id);
                        w.issn.push(res.rows[j].pub_issn);
                        w.types.push(res.rows[j].pub_type);
                   }
                   data.works.push(w);
                   if(i == aux_res_count-1){ client.end(); callback(null, data);}
                });
            }
        });
    });
}

function store_local(data){
    if(data == null){return;}
    var client = new Client({
        user: "orcid_client",
        host: "127.0.0.1",
        database: "orcid",
        password: "orcidpwd",
        port: "5432",
    })

    client.connect()
    query_text = "INSERT INTO orcid.publication(issn, title, sjr) VALUES($1,$2,$3)"
        for(let i = 0; i < data.pubs.issn.length; i++){
            query_values = [data.pubs.issn[i],data.pubs.titles[i],data.pubs.sjr[i]];
            client.query(query_text, query_values, (err, res) => {
            });
        }
    
    var author_id = 0;
    
    var query_text = "INSERT INTO orcid.author(orcid,author_name,last_update) values($1, $2, $3) RETURNING author_id";
    var query_values = [data.author.orcid, data.author.name, data.author.last_update];
    var author_id;
    client.query(query_text, query_values, (err, res) => {
        author_id = res.rows[0].author_id;
        
        query_text = "INSERT INTO orcid.work(title) VALUES($1) RETURNING work_id";
        for(let i = 0; i < data.works.length; i++){
            query_values = [data.works[i].title];
            client.query(query_text, query_values, (err, res) => {
                
                let current_work_id = res.rows[0].work_id;
                query_text_wa = "INSERT INTO orcid.work_author(author, parent_work) VALUES($1,$2)";
                query_values_wa = [author_id, current_work_id];
                client.query(query_text_wa, query_values_wa, (err, res) => {
                });
                
                query_text_wp = "INSERT INTO orcid.work_publication(put_code, parent_work, pub_issn, pub_year, scopus_id, dblp_id, wos_id, pub_type) VALUES($1,$2,$3,$4,$5,$6,$7,$8)"
                for(let j = 0; j < data.works[i].put_codes.length; j++){
                    query_values_wp = [data.works[i].put_codes[j], current_work_id, data.works[i].issn[j], data.works[i].years[j], data.works[i].scopus_ids[j], data.works[i].dblp_ids[j], data.works[i].wos_ids[j], data.works[i].types[j] ];
                    client.query(query_text_wp, query_values_wp, (err, res) => {
                        if(i == data.works.length -1 && j == data.works[i].put_codes.length -1) client.end();
                    });
                }

            });
        }
    });
}

function fetch_data(author, callback){
    
    var client = new Client({
        user: "orcid_client",
        host: "127.0.0.1",
        database: "orcid",
        password: "orcidpwd",
        port: "5432",
    })

    
    request({
        url: "https://pub.orcid.org/v2.1/"+author.orcid+"/record/",
        method: "GET",
        headers: {Accept: "application/json", Authorization:"Bearer "+orcid_access_token} //vnd.orcid+xml
    }, function (error, response, body){
        
            if(response.statusCode != 200){ 
                console.error("Error " + response.statusCode + " on fetching OrcID 1");
                callback(error);
                return;
            };
            
            response_data = JSON.parse(body);
            let name = " ";
            if(response_data["person"]["name"]["family-name"] != null) name += response_data["person"]["name"]["family-name"]["value"]; 
            if(response_data["person"]["name"]["given-names"] != null) name += " , " + response_data["person"]["name"]["given-names"]["value"];
            author.name = name;
            
            author.last_update = response_data["history"]["last-modified-date"]["value"];
            client.connect();
            client.query("SELECT last_update FROM orcid.author WHERE orcid = $1", [author.orcid], (err, res) => {
               if(res.rows.length == 0){ fetch_from_api_orcid(author, callback);}
               else if(res.rows[0].last_update == author.last_update){ fetch_from_local(author, callback);}
               else{ fetch_from_api_orcid(author, callback);}
               client.end();
            });
            
    });
    
    //fetch_from_local(author, callback);
}
    
    var authors = [];
    
    
    app.listen(8081, () => console.log('Listening on port 8081!'));
    
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    
    app.get('/', function (req, response) {
        var orcids = req.query.orcids;
        orcids = orcids.replace(/\s+/g, '');  //remove spaces
        orcids = orcids.split(",");            //split orcids
        orcids = orcids.slice(0,10);            //limit to 10
        authors = orcids.map(function(currentValue){
           aut = new Object();
           aut.orcid = currentValue;
           return aut;
        });
        
        async.concat(authors, function(item, callback){
            fetch_data(item, callback);
        }, function(err,res){
            response.send(JSON.stringify(res));
        });
        
    });
    
    
    
     
    
